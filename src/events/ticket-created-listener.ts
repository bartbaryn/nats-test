import { Message } from 'node-nats-streaming';
import { Listener, Subjects, TicketCreatedEvent } from '@babartickets/common';

export class TicketCreatedListener extends Listener<TicketCreatedEvent> {
  readonly subject:  Subjects.TicketCreated = Subjects.TicketCreated;
  queueGroupName = 'payments-service';

  onMessage(data: TicketCreatedEvent['data'], message: Message) {
    console.log('Event data!', data);

    message.ack();
  }
}
